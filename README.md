# StickFighter - Map Creator Web Tool

* AABB Quadtree based Map Creator & more !  
* [**Here you can try it !**](https://quentinraymondaud.itch.io/stickfighter-map-creator)

![Stickfighter_img_testmap](https://gitlab.com/Raymondaud.Q/stickfighter/-/raw/main/StickFighter_MapCreator/img/default.png)


# StickFighter - Asset Creator Web Tool

* 2D Stickman animation engine, Asset Creator & more !  
* [**Here you can try it !**](https://quentinraymondaud.itch.io/stickfighter-asset-creator)

![Stickfighter_img_1](https://gitlab.com/Raymondaud.Q/stickfighter/-/raw/main/StickFighter_AssetCreator/img/cptr.png)
![Stickfighter_gif_1](https://gitlab.com/Raymondaud.Q/stickfighter/-/raw/main/StickFighter_AssetCreator/img/cptr.gif)
![Stickfighter_img_2](https://gitlab.com/Raymondaud.Q/stickfighter/-/raw/main/StickFighter_AssetCreator/img/cptr2.png)


# Dev - Dependencies

- [P5js](https://p5js.org/)
- [ExpressJs](https://expressjs.com/)
- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Dev - Dependencies that you must install

- [NodeJS & NPM](https://nodejs.org/en/download/package-manager/)

## Dev - Build & Run a projet :

- Download this rep
- Extract Files
- cd into project's folder
- Run `npm i`
- Run `node server.js`
- Go to localhost:3000
