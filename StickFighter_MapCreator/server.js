const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
	res.sendFile('index.html', {root: __dirname })
})

app.get('/main', (req,res) => { 
	res.sendFile('main.js', {root: __dirname})
})

app.get('/enums', (req,res) => { 
	res.sendFile('enums.js', {root: __dirname})
})

app.use('/textures', express.static('textures'))
app.use('/engine', express.static('engine'))
app.use('/lib', express.static('lib'))
app.use('/style', express.static('style'))

app.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})
