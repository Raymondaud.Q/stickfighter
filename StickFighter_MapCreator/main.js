var stickmap;
var pressedKeys=[]
var clickPos
var unClickPos
var btnWidget
var translateBuffer = [0,0]
var mouseAABB;
var noPhysics = true
var platformTypes = [STD,STICKY,SLIPPERY,MOVABLE,BREAKABLE,GRAVITY,EXPLODABLE]
var typeIndex = 0
var camLock = true
var canvas;
var creatingBlock = false
var textures =  new Map()
var runFrameIndex = 0
var right=true
var windowResizing = false
var nbFrame = 0
var previousRoll = false


function preload() {

	textures.set(STD, loadImage('textures/std.png'))
	textures.set(STICKY, loadImage('textures/sticky.png'))
	textures.set(SLIPPERY, loadImage('textures/slippery.png'))
	textures.set(MOVABLE, loadImage('textures/movable.png'))
	textures.set(BREAKABLE, loadImage('textures/breakable.png'))
	textures.set(EXPLODABLE, loadImage('textures/explodable.png'))
	textures.set(GRAVITY, loadImage("textures/gravity.png"))

}

function download() {
		var file = new Blob([JSON.stringify({elems:(stickmap.elems)})], {type: "json"});
		let filename = "MAP.json"
		if (window.navigator.msSaveOrOpenBlob) // IE10+
				window.navigator.msSaveOrOpenBlob(file, filename);
		else { // Others
				var a = document.createElement("a"),
								url = URL.createObjectURL(file);
				a.href = url;
				a.download = filename;
				document.body.appendChild(a);
				a.click();
				setTimeout(function() {
						document.body.removeChild(a);
						window.URL.revokeObjectURL(url);  
				}, 0); 
		}
}


function handleFile(files) {
 var file = files.file;
	if (!file) {
		return;
	}
	var reader = new FileReader();
	reader.onload = function(e) {
		var data = JSON.parse(e.target.result)
		stickmap = new StickMap(mapWidth, mapHeight);
		for (let i in data.elems){
			let elem = new Platform(data.elems[i].vertices, data.elems[i].type)
			stickmap.addElem(elem)
		}
	};
	reader.readAsText(file);
}


async function setup() {
	canvas = await createCanvas(windowWidth, windowHeight, WEBGL);
	for (let element of document.getElementsByClassName("p5Canvas")) {
		element.addEventListener("contextmenu", (e) => e.preventDefault());
	}
	strokeWeight(2);
	//Stroke with a semi-transparent white
	//stroke(255, 160);
	//angleMode("DEGREES")
	textFont(fontStr)
	initBtns(); 
	translateBuffer = [-mapWidth/2,-mapHeight/2]
	mouseAABB = new AxisAlignedBoundingBox([-mapWidth/2,-mapHeight/2],100,100)
	initMap()
	frameRate(50)
	setInterval(()=>{
		keyHandled()
		stickmap.update()
	},18)
}

function initBtns() {
	let nbButtonsPerSlot = 11
	btnWidget = new ButtonWidget(
		posX=0,
		posY=0,
		width=((canvas.width-(canvas.width/nbButtonsPerSlot))/nbButtonsPerSlot),
		height=((canvas.height-(canvas.height/nbButtonsPerSlot))/nbButtonsPerSlot),
		font="monospace",
		fontWeigth="bold",
		fontSize="12px",
		padding=0,
		rotate=1,
		backgroundColor="#72787e",
		scBgColor="#3b88c3",
		canvasWidth=canvas.width,
		canvasHeight=canvas.height
	);
	btnWidget.addButton("FPS", "FPS: "+ str(getFrameRate()).slice(0,4), ()=>{btnWidget.updateText('FPS', "FPS: "+ str(getFrameRate()).slice(0,4))})
	btnWidget.addFileInput("loadBtn","ok", handleFile)  
	btnWidget.addButton("saveBtn","[O]-Save Map",() =>{
		download()
	})
	btnWidget.addButton("reinit","Default Map", ()=>{initMap()})
	btnWidget.addButton("clear","Clear Map", ()=>{stickmap = new StickMap(mapWidth, mapHeight);})
	btnWidget.addButton("del","[⌫]-Suppr.", ()=>{stickmap.removeSelected()})
	btnWidget.addButton("add","[C]-Create", ()=>{createBloc()})
	btnWidget.addButton("platformType","[T]-Plateform "+platformTypes[typeIndex]  , ()=>{
		typeIndex = (typeIndex + 1) % platformTypes.length; 
		btnWidget.updateText("platformType","[T]-Plateform "+platformTypes[typeIndex])
	})
	btnWidget.addButton("collideMode","[P]-Enable Physics", ()=>{
		noPhysics=!noPhysics
		btnWidget.updateText("collideMode","[P]-"+(noPhysics?"Enable Physics":"Disable Physics") )
	})
	btnWidget.addButton("respawnBtn","[R]-Respawn Stick", ()=>{
		stickmap.stick.setPosition([200,200]) 
	})
	btnWidget.addButton("camLock", "[L]-"+(camLock ? "Unlock Cam":"Lock Cam"), ()=>{
		camLock = !camLock
		btnWidget.updateText("camLock", "[L]-"+(camLock ? "Unlock Cam":"Lock Cam"))
	})
	btnWidget.addButton("camUp","[⬆]-CAM UP", ()=>{translateBuffer[1]+=10})
	btnWidget.addButton("camDown","[⬇]-CAM DOWN", ()=>{translateBuffer[1]-=10})
	btnWidget.addButton("camLeft","[⬅]-CAM LEFT", ()=>{translateBuffer[0]+=10})
	btnWidget.addButton("camRight","[➡]-CAM RIGHT", ()=>{translateBuffer[0]-=10})
	btnWidget.addButton("stickUp","[Z]-Stick UP", ()=>{stickUp()})
	btnWidget.addButton("stickDown","[S]-Stick DOWN", ()=>{stickDown()})
	btnWidget.addButton("stickLeft","[Q]-Stick LEFT", ()=>{stickLeft()})
	btnWidget.addButton("stickRight","[D]-Stick RIGHT", ()=>{stickRight()})
}

function initMap(){
	stickmap = new StickMap(mapWidth, mapHeight);
	setTimeout(() => {
		for (let i in preFilledMap)
			stickmap.addElem(new Platform(preFilledMap[i].vertices, preFilledMap[i].type))
	}, 100)
}


function createBloc(){
	if (!creatingBlock){
		creatingBlock = true
		let lowerX = 0
		let higherX = 0
		let higherY = 0
		let lowerY = 0

		if (clickPos[0] < unClickPos[0]){
			lowerX = clickPos[0]
			higherX = unClickPos[0]
		} else {
			lowerX = unClickPos[0]
			higherX = clickPos[0]
		}

		if (clickPos[1] < unClickPos[1]){
			lowerY = clickPos[1]
			higherY = unClickPos[1]
		} else {
			lowerY = unClickPos[1]
			higherY = clickPos[1]
		}

		clickPos[0]=lowerX
		clickPos[1]=lowerY
		unClickPos[0]=higherX
		unClickPos[1]=higherY
		stickmap.addElem(new Platform([clickPos.map(elem => elem),unClickPos.map(elem => elem)],platformTypes[typeIndex]))
		clickPos = undefined
		unClickPos = undefined
		creatingBlock = false
	}
}

function mousePressed(event) {
	mouseX = mouseX - canvas.width/2
	mouseY = mouseY - canvas.height/2
	clickPos = undefined
	unClickPos = undefined
	if (stickmap != undefined ){
		stickmap.mouseMoved(mouseX-translateBuffer[0],mouseY-translateBuffer[1])
		stickmap.mouseClicked(mouseX,mouseY)
	}
}

function mouseReleased() {}

function mouseDragged(event) {
	if(canvas && mouseAABB){
		mouseX = mouseX - canvas.width/2
		mouseY = mouseY - canvas.height/2
		mouseAABB.move([mouseX-translateBuffer[0],mouseY-translateBuffer[1]])
		if (event.buttons == 1){
			if (clickPos == undefined)
				clickPos = [mouseX-translateBuffer[0],mouseY-translateBuffer[1]]
			unClickPos = [mouseX-translateBuffer[0],mouseY-translateBuffer[1]]
		}
	}
}

function mouseMoved() {
	if(canvas){
		mouseX = mouseX - canvas.width/2
		mouseY = mouseY - canvas.height/2
		if(noPhysics)
			mouseAABB.move([mouseX-translateBuffer[0],mouseY-translateBuffer[1]])
		if (clickPos == undefined && stickmap != undefined){
			stickmap.mouseMoved(mouseX-translateBuffer[0],mouseY-translateBuffer[1])
		}
	}
}

function keyPressed(){
	pressedKeys[keyCode]=1
	if (keyCode==C && clickPos != undefined && unClickPos != undefined )
		createBloc()

	else if (keyCode==SUPPR || keyCode==ERASE )
		stickmap.removeSelected()

	else if (keyCode==R) // Respawn
		stickmap.stick.setPosition([200,200]) 

	else if (keyCode==P){ // Toggle Physics
		noPhysics=!noPhysics
		btnWidget.updateText("collideMode", "[P]-"+(noPhysics?"Enable Physics":"Disable Physics"))
	}

	else if (keyCode==T){ // Platform type shift
		typeIndex = (typeIndex + 1) % platformTypes.length; 
		btnWidget.updateText("platformType","[T]-Plateform "+platformTypes[typeIndex])    
	}

	else if (keyCode == O)// Save
		download()

	else if(keyCode == I)// Import
		btnWidget.buttons.get("loadBtn").elt.click()

	else if (keyCode == L){// CamLock mode swap
		camLock = !camLock
		btnWidget.updateText("camLock", "[L]-"+(camLock ? "Unlock Cam":"Lock Cam"))
	}
}

function stickUp(){
	if (!noPhysics && stickmap.stick.grounded && !stickmap.stick.roll){
		stickmap.stick.speedY = JUMP_FORCE
		stickmap.stick.grounded = false
		stickmap.stick.fallStart = performance.now()
	} else if (noPhysics){
		stickmap.stick.drag(0,-SPEED_AMOUNT)

	}
}

function stickDown(){
	if (!noPhysics && !stickmap.stick.roll)
		stickmap.stick.speedY = SPEED_AMOUNT
	else if (noPhysics){
		stickmap.stick.drag(0,SPEED_AMOUNT)
	}
}

function stickLeft(){
	if (noPhysics)
		stickmap.stick.drag(-SPEED_AMOUNT,0)
	else
		stickmap.stick.speedX = -SPEED_AMOUNT

	let tmPos = stickmap.stick.aabb.xy
	if (! stickmap.stick.roll){
		stickmap.stick.setFrame(preFilledLeft[runFrameIndex])
		runFrameIndex = (runFrameIndex+1)%(int(preFilledLeft.length/2+3))
	}
	stickmap.stick.setPosition(tmPos)
	right=false

}

function stickRight(){
	if (noPhysics)
		stickmap.stick.drag(SPEED_AMOUNT,0)
	else
		stickmap.stick.speedX = SPEED_AMOUNT

	let tmPos = stickmap.stick.aabb.xy
	if (! stickmap.stick.roll){
		stickmap.stick.setFrame(preFilledRight[runFrameIndex])
		runFrameIndex = (runFrameIndex+1)%(int(preFilledRight.length/2+3))
	}
	stickmap.stick.setPosition(tmPos)
	right=true
}

function keyReleased(){ pressedKeys[keyCode]=0 }

function windowResized() {
	if(!windowResizing){
		windowResizing = true
		resizeCanvas(windowWidth, windowHeight);
		btnWidget.clear()
		initBtns()
		windowResizing = false
	}
}

function keyHandled(){
	let roll = stickmap.stick.roll


	if (pressedKeys[Z] && !pressedKeys[S])
		stickUp()
	else if (pressedKeys[S] && !pressedKeys[Z] )
		stickDown()


	if (pressedKeys[D] && !pressedKeys[Q]){
		stickRight()
	} else if (pressedKeys[Q] && !pressedKeys[D] ){
		stickLeft()
	}  else if (!roll) { // IDLE Frame
		let tmPos = stickmap.stick.aabb.xy
		if(right)
			stickmap.stick.setFrame(preFilledRight[0])
		else      
			stickmap.stick.setFrame(preFilledLeft[0])
		stickmap.stick.setPosition(tmPos)
	} else if (roll && !noPhysics){
		!right ? stickLeft() : stickRight()
	}

	if (pressedKeys[UP] && !pressedKeys[DOWN])
		translateBuffer[1]+=10
	else if (pressedKeys[DOWN])
		translateBuffer[1]-=10

	if (pressedKeys[LEFT] && !pressedKeys[RIGHT])
		translateBuffer[0]+=10
	else if (pressedKeys[RIGHT] )
		translateBuffer[0]-=10
}

function draw() {
	if(camLock){
		let diffX = -(stickmap.stick.aabb.xy[0]+translateBuffer[0])/SMOOTH_CAMFACTOR
		let diffY = -(stickmap.stick.aabb.xy[1]+translateBuffer[1])/SMOOTH_CAMFACTOR
		translateBuffer[0]+=diffX
		translateBuffer[1]+=diffY
	}

	let roll = stickmap.stick.roll
	if (roll) {
		if (!previousRoll)
			runFrameIndex = 0
		let tmPos = stickmap.stick.aabb.xy
		let anim = !right ? preFilledDownLeft : preFilledDownRight
		if (!noPhysics){
			stickmap.stick.setFrame(anim[runFrameIndex])
			runFrameIndex = (runFrameIndex+1)%(int(anim.length))
			if (runFrameIndex >= anim.length-1 ){
				stickmap.stick.roll = false
			}
		}
		stickmap.stick.setPosition(tmPos)
	}
	previousRoll = roll

	translate(translateBuffer[0],translateBuffer[1])
	background("#c7cbd1");
	fill(255,0,0,50)
	if (clickPos != undefined && unClickPos != undefined)
		rect(clickPos[0],clickPos[1],unClickPos[0]-clickPos[0],unClickPos[1]-clickPos[1])
	fill(0,0,0,200)
	stickmap.show()
	if (noPhysics)
		mouseAABB.show()

	fill(0);
	if (nbFrame > getFrameRate()){
		nbFrame=0
		btnWidget.updateText('FPS', "FPS: "+ str(getFrameRate()).slice(0,4) )
	} else {
		nbFrame += 1
	}
}
