// Quentin Raymondaud V2
// -- Unlicence

/* Usage example
  let nbButtonsPerSlot = 10
  btnWidget = new ButtonWidget(
    posX=0,
    posY=0,
    width=((windowWidth-(windowWidth/nbButtonsPerSlot))/nbButtonsPerSlot),
    height=50,
    font="monospace",
    fontWeigth="bold",
    fontSize="15px",
    padding=0,
    rotate=1,
    backgroundColor="#72787e",
    scBgColor="#3b88c3"
  );
*/

class ButtonWidget{
	constructor(
			posX=0,
			posY=0,
			width=100,
			height=30,
			font="monospace",
			fontWeigth="bold",
			fontSize="12px",
			padding=5,
			rotate=0,
			backgroundColor="#72787e",
			scBgColor="#3b88c3",
			canvasWidth=100,
			canvasHeight=100
		){



		this.x = posX
		this.y = posY

		this.width = width 
		this.height = height

		this.padding = padding

		this.buttons = new Map()
		this.buttonsArray = []
		this.rotate = rotate

		this.font = font
		this.fontWeigth = fontWeigth
		this.fontSize = fontSize

		this.backgroundColor = backgroundColor

		this.prevBtn = createButton((this.rotate?"⬅":"⬆"))
		this.prevBtn.mousePressed(()=>{
			this.index-=1
			if (this.index < 0)
				this.index = Math.floor(this.buttonsArray.length / this.visiblePosition.length)
			this.changeSlot(this.index)
		})

		let widthA = 0
		let heightA = 0
		if ( this.rotate ){
			widthA = this.width/2
			heightA = this.height 
		} else {
			widthA = this.width
			heightA = this.height/2
		}

		this.prevBtn.style('font-weight', this.fontWeigth)
		this.prevBtn.style('font-family', this.font)
		this.prevBtn.style('font-size', this.fontSize)
		this.prevBtn.style('background-color', scBgColor)
		this.prevBtn.position( this.x+this.padding,this.y+this.padding)
		this.prevBtn.size(widthA,heightA)

		let x = 0;
		let y = 0;
		if (!this.rotate){
			x = this.x + this.padding
			y = canvasHeight - (heightA + this.padding) + this.y
		} else {
			x = canvasWidth - (widthA + this.padding) + this.x
			y = this.y + this.padding 
		}

		this.nextBtn = createButton((this.rotate?"➡":"⬇"))
		this.nextBtn.mousePressed(()=>{
			this.index+=1
			if (this.index > Math.floor(this.buttonsArray.length / this.visiblePosition.length))
				this.index = 0
			this.changeSlot(this.index)
		})

		this.nextBtn.style('font-weight', this.fontWeigth)
		this.nextBtn.style('font-family', this.font)
		this.nextBtn.style('font-size', this.fontSize)
		this.nextBtn.style('background-color', scBgColor)
		this.nextBtn.position(x, y)
		this.nextBtn.size(widthA,heightA)
		
		this.index=0
		this.initVisibleSlot()
		this.changeSlot(this.index)
	}

	initVisibleSlot(){
		this.visiblePosition = []
		for ( let  i = 0; i < 1000; i++){
			let posX = 0
			let posY = 0
			let realPadding = (i*this.padding)+this.padding
			if (!this.rotate){
				posX = this.x+this.padding
				posY = (this.height/2)+this.y+realPadding+(i*this.height)+this.padding

				if ( posY + this.height > this.nextBtn.y )
					break
				else 
					this.visiblePosition.push({x:posX, y:posY})
			} else {
				posX = (this.width/2)+this.x+realPadding+(i*this.width)+this.padding
				posY = this.y+this.padding
				
				if ( posX + this.width > this.nextBtn.x)
					break
				else 
					this.visiblePosition.push({x:posX, y:posY})
			}
		}
	}

	changeSlot(index){
		if (this.buttonsArray.length > 0){
			let slotSize = this.visiblePosition.length
			this.buttons.forEach (function(value, key) {
				if (value != undefined){
					value.position(0,0)
					value.hide()
				}
			})

			for (let iVisible = 0; iVisible < slotSize; iVisible++){
				let position = this.visiblePosition[iVisible]
				let btn = this.buttonsArray[iVisible+(slotSize*index)]
				if (btn != undefined){
					btn.position(position.x, position.y)
					btn.show()
				}
			}
		}
	}

	addButton(id,text,func){

		let newBtn = createButton(text)
		newBtn.mousePressed(func)
		newBtn.size(this.width,this.height)
		newBtn.style('font-family', this.font)
		newBtn.style('font-weight', this.fontWeigth)
		newBtn.style('font-size', this.fontSize)
		newBtn.style('background-color', this.backgroundColor)

		this.buttons.set(id,newBtn)
		this.buttonsArray[this.buttons.size-1]=newBtn
		this.changeSlot(this.index)
	}

	addFileInput(id,text,func){
		let newBtn = createFileInput(func,text)
		newBtn.size(this.width,this.height)
		newBtn.style('font-family', this.font)
		newBtn.style('font-weight', this.fontWeigth)
		newBtn.style('font-size', this.fontSize)
		newBtn.style('background-color', this.backgroundColor)

		this.buttons.set(id,newBtn)
		this.buttonsArray[this.buttons.size-1]=newBtn
		this.changeSlot(this.index)
	}

	updateText(id,text){
		this.buttons.get(id).elt.textContent = text
	}

	clear(){
		this.prevBtn.remove()
		this.nextBtn.remove()
		for(let btn in this.buttonsArray){
			this.buttonsArray[btn].remove()
		}
		this.buttonsArray = []
		this.buttons = {}
	}

	show(id,show){
		let btn = this.buttons.get(id)
		if (btn != undefined ){
			if (show)
				btn.show()
			else
				btn.hide()
		}
	}
}