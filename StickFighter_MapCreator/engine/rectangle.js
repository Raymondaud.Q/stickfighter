class Rectangle extends AxisAlignedBoundingBox{

	static aabbToRect(aabb){
		return new Rectangle([aabb.xy[0]-aabb.width, aabb.xy[1]-aabb.height],[aabb.xy[0]+aabb.width, aabb.xy[1]+aabb.height])
	}

	constructor(verts){
		let width = (verts[1][0] - verts[0][0])/2
		let height = (verts[1][1] - verts[0][1])/2
		super([verts[0][0]+width, verts[0][1]+height], width, height)
		this.vertices = verts
		this.overred = false
		this.selected = false
		this.hasMoved = false
		this.toRemove = false
		this.scheduledMove = undefined
	}

	move(newXY){
		if ( newXY.length == 2 ){
			super.move(newXY)
			this.vertices=[[this.xy[0]-this.witdh,this.xy[1]-this.height],[this.xy[0]+this.witdh,this.xy[1]+this.height]]
		}
	}

	show(){
		circle(this.vertices[0][0],this.vertices[0][1],10)
		circle(this.vertices[1][0],this.vertices[1][1],10)
		rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
		super.show()
	}
}


class Platform extends Rectangle{
	constructor(verts,type=STD){
		super(verts)
		this.type=type
		if (this.type == MOVABLE){
			this.top += this.height
		}
		this.hasMoved=false
	}

	move(newXY){
		if ((this.type == MOVABLE || this.type == GRAVITY) && newXY.length == 2){
			super.move(newXY)
		}
	}

	show(){
		let u = 0
		let v = 0
		push()
		if(this.type == STD){
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
			textureWrap(REPEAT,MIRROR)
			texture(textures.get(this.type))
			u = lerp(1, 2, this.xy[0]);
			v = lerp(0, 0.5, this.xy[1]);
			this.shape(u,v);
		} else if(this.type==STICKY){
			noFill()
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
			textureWrap(REPEAT,REPEAT)
			texture(textures.get(this.type))
			u = lerp(0, 0.5, this.xy[0]);
			v = lerp(1, 2, this.xy[1]);
			this.shape(u,v);
		} else if(this.type == SLIPPERY){
			if(noPhysics)
				rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
			textureWrap(REPEAT,MIRROR)
			texture(textures.get(this.type))
			u = lerp(0, 0.5, this.xy[0]);
			v = lerp(1, 1.1, this.xy[1]);
			this.shape(u,v);
		} else if(this.type == MOVABLE){
			texture(textures.get(this.type))
			if(!noPhysics)
				noStroke()
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
		} else if (this.type == BREAKABLE){
			texture(textures.get(this.type))
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
		} else if(this.type == GRAVITY){
			texture(textures.get(this.type))
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
		} else if(this.type == EXPLODABLE){
			texture(textures.get(this.type))
			rect(this.vertices[0][0],this.vertices[0][1],this.vertices[1][0]-this.vertices[0][0],this.vertices[1][1]-this.vertices[0][1])
		} else {
			super.show()
		}
		pop()
	}

	shape(u,v){
		beginShape();
			vertex( this.left,this.top, 0, 0, 0);
			vertex( this.right,this.top,  0, u, 0);
			vertex( this.right,this.bot, 0, u, v);

			vertex(this.right,this.bot, 0, u, v);
			vertex(this.left,this.bot,  0, 0, v);
			vertex(this.left,this.top,  0, 0, 0);
		endShape();
	}
}

