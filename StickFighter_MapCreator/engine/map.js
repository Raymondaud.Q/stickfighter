var rect_timeout = undefined

class StickMap {

	constructor(width, height) {
		this.width = width;
		this.height = height;
		this.elems = []
		this.qTree = new AABBQuadTree([this.width,this.height],this.width,this.height)
		this.stick = new Stickman()
		this.stick.setPosition([200,200])
	}

	addElem(elem){
		if (this.elems.indexOf(elem) < 0)
			this.elems.push(elem)
		this.qTree.insert(elem)
	}

	removeSelected(){
		for (let elem in this.elems)
			if (this.elems[elem].selected)
				this.elems[elem].toRemove = true
	}

	start() {}

	clear() {}

	mouseMoved(mouseX,mouseY){
		for (let elem in this.elems)
			if (this.elems[elem].contains(mouseAABB.xy))
				this.elems[elem].overred = true
			else
				this.elems[elem].overred = false
	}

	mouseDraggedd(mouseX, mouseY){
	}

	mouseClicked(mouseX,mouseY){
		for (let elem in this.elems)
			if (this.elems[elem].overred)
				this.elems[elem].selected = !this.elems[elem].selected
	}

	displayParams(elem){
		if (noPhysics){
			let res = this.qTree.AABBQuery(this.stick.aabb)
			res = [...res, ...this.qTree.AABBQuery(mouseAABB)]
			stroke("#000")
			if (elem.selected)
				stroke(0,200,0,45)
			else if(elem.overred)
				stroke(0,0,200,45)
			else if (res.includes(elem))
				stroke(250,0,0,45)
		} else 
			stroke("#000")
	}

	show() {
		this.stick.show()
		let newElems = []
		let newQtree = new AABBQuadTree([this.width,this.height],this.width,this.height)
		for (let iElem in this.elems){
			let elem = this.elems[iElem]   
			this.displayParams(elem)
			elem.show()
		}
		if (noPhysics){
			mouseAABB.show()
			this.qTree.show()
		}
	}

	update(){
		if (!noPhysics)
			this.stick.dragAndCollide(this.qTree)
		let newElems = []
		let newQtree = new AABBQuadTree([this.width,this.height],this.width,this.height)
		for (let iElem in this.elems){
			let elem = this.elems[iElem]   
			if (!elem.toRemove){
				elem = this.processElemStates(elem);
				newElems.push(elem)
				newQtree.insert(elem)
			}
		}
		this.elems = newElems
		this.qTree = newQtree
	}

	processElemStates(elem) {
		if (elem.type == GRAVITY){
			let result = this.qTree.AABBQuery(elem)
			let onlyThis = true
			for (let i in result){
				if (result[i] !== elem){
					onlyThis = false
					if (result[i].type==BREAKABLE && rect_timeout == undefined && elem.hasMoved)
						rect_timeout = setTimeout(() => {
							result[i].toRemove = true;
							rect_timeout = undefined
						}, 250)
				}
			}
			if (!noPhysics && onlyThis){
				elem = this.moveElem(elem,GRAVITY,0,GRAVITY_AMOUNT)
				elem.hasMoved=true
				if (this.stick.aabb.intersects(elem) && this.stick.aabb.bot > elem.bot && elem.hasMoved)
					this.stick.setPosition([200,200])
			}
		} else if (elem.type == MOVABLE && elem.scheduledMove && elem.scheduledMove.length > 0)
			elem = this.moveElem(elem,MOVABLE,elem.scheduledMove[0],elem.scheduledMove[1])
		return elem;
	}

	moveElem(elem,type,offsetX,offsetY){
		let newXY = [elem.xy[0]+offsetX, elem.xy[1]+offsetY]
		let newVertices = [[newXY[0]-elem.width, newXY[1]-elem.height], [newXY[0]+elem.width, newXY[1]+elem.height]]
		let newPlat = new Platform(newVertices,type)
		return newPlat;
	}
}
