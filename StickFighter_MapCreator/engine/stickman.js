class Stickman {

	toPolarEdges(){
		let polarEdges = []
		for ( let iTopo in this.topo){
			let vertA = this.vertices[this.topo[iTopo][0]]
			let vertB = this.vertices[this.topo[iTopo][1]]
			let ctp = cartesianToPolar(vertB,vertA)
			let r_ctp = cartesianToPolar(vertA,vertB)
			polarEdges[iTopo]={vertexA:vertA, vertexB:vertB, ctp:ctp}
			polarEdges[int(iTopo)+int(this.topo.length)]={vertexA:vertB, vertexB:vertA, ctp: r_ctp}
		}
		return polarEdges
	}

	angle(cx, cy, ex, ey) {
		var dy = ey - cy;
		var dx = ex - cx;
		var theta = atan2(dy, dx);
		return theta;
	}

	constructor(posX=0, posY=0){
		this.vertices = [
			[250,278,0], // 0 Pied Gauche     
			[258,257,0], // 1 Genoux Gauche   
			[272,278,0], // 2 Pied Droit      
			[270,257,0], // 3 Genoux Droit   
			[260,240,0], // 4 Hanche   
			[281,225,0], // 5 Coude Gauche    
			[282,207,0], // 6 Main Gauche     
			[274,229,0], // 7 Coude Droit     
			[274,206,0], // 8 Main Droite     
			[260,210,1]  // 9 Epaules 
		]

		this.topo = [
			[0,1],
			[2,3],
			[1,4],
			[3,4],
			[9,4],
			[5,9],
			[7,9],
			[5,6],
			[7,8]
		]

		//this.polarEdges = this.toPolarEdges()
		this.overredVertex = null;
		this.vertexEdit = null;
		this.selectedVertices = [];
		this.angle = radians(-90);
		this.sym = false
		this.speedX = 0
		this.speedY = 0
		//this.aabb = Rectangle.aabbToRect(new AxisAlignedBoundingBox(this.vertices[4],50,50))
		this.aabb = new AxisAlignedBoundingBox([this.vertices[4][0],this.vertices[4][1]],10,35)
	}


	setHead(){
		if (this.vertexEdit != null ) 
			this.vertices[this.vertexEdit][2] = ( this.vertices[this.vertexEdit][2]==0? 1:0)
		else if (this.overredVertex != null) 
			this.vertices[this.overredVertex][2] = ( this.vertices[this.overredVertex][2]==0? 1:0)
	}

	setFrame(frame){
		if (frame != null){
			this.topo = frame.topo
			this.vertices = frame.vertices
		}
		//this.polarEdges = this.toPolarEdges()
	}


	rotate(cx,cy){
		if (this.vertexEdit != null ){
			let pivotX = this.vertices[this.vertexEdit][0]
			let pivotY = this.vertices[this.vertexEdit][1]

			let angle = Math.atan2(cy-pivotY, cx-pivotX)
			let cosA = cos(this.angle - angle)
			let sinA = -sin(this.angle - angle)
			this.angle = angle

			for (let iSelected in this.vertices){
				this.vertices[iSelected][0] -= pivotX
				this.vertices[iSelected][1] -= pivotY
				let offsetX = this.vertices[iSelected][0] * cosA - this.vertices[iSelected][1] * sinA 
				let offsetY = this.vertices[iSelected][0] * sinA + this.vertices[iSelected][1] * cosA 
				this.vertices[iSelected][0] = offsetX + pivotX
				this.vertices[iSelected][1] = offsetY + pivotY
			}
		}
	}

	setPosition(xy){
		let iPos = [this.vertices[4][0], this.vertices[4][1]]
		let dragX = xy[0] - iPos[0] 
		let dragY = xy[1] - iPos[1]
		for (let iSelected in this.vertices){
			this.vertices[iSelected][0] += dragX
			this.vertices[iSelected][1] += dragY
		}
		this.aabb.move([this.vertices[4][0],this.vertices[4][1]])
	}

	drag(offsetX, offsetY){
		for (let iSelected in this.vertices){
			this.vertices[iSelected][0] += offsetX
			this.vertices[iSelected][1] += offsetY
		}
		this.aabb.move([this.vertices[4][0],this.vertices[4][1]])
	}

	dragAndCollide(qTree){
		let res = qTree.AABBQuery(this.aabb)
		let offsetY = this.speedY 
		let offsetX = this.speedX
		let portion = this.speedY/10
		this.speedY -= portion
		offsetY += GRAVITY_AMOUNT


		let leftAABB = new AxisAlignedBoundingBox([this.aabb.left-offsetX,this.aabb.xy[1]],this.aabb.width,this.aabb.height) 
		let rightAABB = new AxisAlignedBoundingBox([this.aabb.right-offsetX,this.aabb.xy[1]],this.aabb.width,this.aabb.height) 
		let topAABB = new AxisAlignedBoundingBox([this.aabb.xy[0],this.aabb.top-offsetY],this.aabb.width,this.aabb.height)
		let botAABB = new AxisAlignedBoundingBox([this.aabb.xy[0],this.aabb.bot-offsetY],this.aabb.width,this.aabb.height) 

		let axisXCollided=false
		let axisYCollided=false
		let slipperyCollided=false
		if ( res.length == 0 ){
			if (this.grounded){
				this.fallStart = performance.now()
				this.grounded = false
			}	
		} else
			for (let col in res){
				const elem = res[col]
				if (!leftAABB.intersects(elem)){
					if ((offsetX > 0 || this.speedX > 0) && elem.type !== SLIPPERY && elem.top < this.aabb.bot){offsetX = 0; this.speedX = 0}
					if ((topAABB.intersects(elem) || botAABB.intersects(elem) && elem.type!==SLIPPERY)) axisXCollided = true
				}
				if (!rightAABB.intersects(elem)){
					if ((offsetX < 0 || this.speedX < 0) && elem.type !== SLIPPERY && elem.top < this.aabb.bot){offsetX = 0; this.speedX = 0}
					if ((topAABB.intersects(elem) || botAABB.intersects(elem) && elem.type!==SLIPPERY)) axisXCollided = true
				}
				if (!topAABB.intersects(elem)){
					if (!this.grounded && 
						(performance.now() - this.fallStart > MS_DELAY_ROLL_AFTER_FALL || offsetY > GRAVITY_AMOUNT)){
						this.roll = true
					}

					if (this.aabb.bot > botAABB.top 
					&& elem.type != MOVABLE
					&& elem.left < this.aabb.left 
					&& elem.right > this.aabb.right ){
						let ajdustedPos = [this.aabb.xy[0], elem.top-this.aabb.height] 
						this.speedY = 0
						this.offsetY = 0
						this.grounded = true
						this.setPosition(ajdustedPos)
					}

					if (offsetY >= 0) {
						this.grounded = true 
						if (!(elem.type==MOVABLE && pressedKeys[M])){
							offsetY = 0
							axisYCollided = true
						}
					}
				}
				if (!botAABB.intersects(elem) && !(elem.type==MOVABLE && pressedKeys[M])){
					if (offsetY < 0) {this.speedY=0; offsetY = 0}
				}


				if (this.aabb.bot >= elem.top && elem.type==STICKY) // Climbable
					this.grounded = true 
				else if (elem.type==SLIPPERY)
					slipperyCollided = true
				else if(elem.type===BREAKABLE){
					setTimeout(() => {
						elem.toRemove = true
					}, BREAKABLE_REMOVAL_DELAY)
				} else if (elem.type==MOVABLE && pressedKeys[M]){
					elem.scheduledMove = [offsetX,offsetY]
				}   
			}

		if ( slipperyCollided && !axisXCollided){
			if (offsetX != 0){
				this.speedX = offsetX
				let bufferX = offsetX / SLIPPERY_FACTOR
				this.speedX -= bufferX
				offsetX += this.speedX
			}
		} else if ( !slipperyCollided && axisYCollided){
			this.speedX = 0
		}

		for (let iSelected in this.vertices){
			this.vertices[iSelected][0] += offsetX
			this.vertices[iSelected][1] += offsetY
		}

		this.aabb.move([this.vertices[4][0],this.vertices[4][1]])
		return res;
	}

	remove(){
		if( this.selectedVertices.length != 0)
			while (this.selectedVertices != null && this.selectedVertices.length != 0)
				for (let i in this.selectedVertices) 
					this.removeVertex(this.selectedVertices[0])
		else if (this.vertexEdit != null)
			this.removeVertex(this.vertexEdit)
	}


	// Utilisé dans symmetrical
	lineCoeff(a,b){
		let result = [];
		result[0] = b[1] - a[1]  // a
		result[1] = a[0] - b[0]  // b
		result[2] = - result[0] *  a[0] - result[1] * a[1];
		return result;
	}
	
	symmetrical() {
		this.sym=!this.sym
		let abc = this.lineCoeff([this.vertices[4][0],0],[this.vertices[4][0],500]);
		for ( let count = 0 ; count < this.vertices.length; count ++ ) {
			this.vertices[count] = [
			 int( -2 * abc[0] * ( abc[0] * this.vertices[count][0] + abc[1] * this.vertices[count][1] + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + this.vertices[count][0]),
			 int( -2 * abc[1] * ( abc[0] * this.vertices[count][0] + abc[1] * this.vertices[count][1] + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + this.vertices[count][1]),
			 this.vertices[count][2]
			];
		}        
	}


	show(){


		// Animation par modification de sommets
		//let randomA = random(-0.01,0.01)
		//let randomB = random(-0.01,0.01)
		/*for (let iTopo in this.topo){
			//this.vertices[this.topo[iTopo][0]][0] += randomA
			//this.vertices[this.topo[iTopo][0]][1] += randomB
			this.vertices[this.topo[iTopo][1]][0] += randomA
			this.vertices[this.topo[iTopo][1]][1] += randomB
		}*/
		// this.polarEdges = this.toPolarEdges()


		for ( let iTopo in this.topo){
			let vertA = this.vertices[this.topo[iTopo][0]]
			let vertB = this.vertices[this.topo[iTopo][1]]
			if (vertA != null && vertB != null) line(vertA[0], vertA[1]+10, vertB[0], vertB[1]+10)
		}

		for ( let iVert in this.vertices)
			if (this.vertices[iVert][2] == 1)
				circle(this.vertices[iVert][0],this.vertices[iVert][1]+5,10)

		fill(0)

		if (noPhysics)
			this.aabb.show()

		 // Animation par modification d'angle en mode Polar Edge
		/*for (let iTopo in this.topo){
			let pet = this.polarEdges[iTopo]
			pet.ctp.angle+=randomA
			let targetX = pet.ctp.distance * cos(pet.ctp.angle)
			let targetY = pet.ctp.distance * sin(pet.ctp.angle)
			this.vertices[this.topo[iTopo][1]][0] = targetX + this.vertices[this.topo[iTopo][0]][0]
			this.vertices[this.topo[iTopo][1]][1] = targetY + this.vertices[this.topo[iTopo][0]][1]

			pet = this.polarEdges[int(iTopo)+int(this.topo.length)]
			pet.ctp.angle+=randomB
			targetX = pet.ctp.distance * cos(pet.ctp.angle)
			targetY = pet.ctp.distance * sin(pet.ctp.angle)
			this.vertices[this.topo[iTopo][0]][0] = targetX + this.vertices[this.topo[iTopo][1]][0]
			this.vertices[this.topo[iTopo][0]][1] = targetY + this.vertices[this.topo[iTopo][1]][1]
		}*/

		// Normal Edge rendering
		/*for ( let iTopo in this.topo){
			let pe = this.polarEdges[iTopo]
			let targetX = pe.ctp.distance * cos(pe.ctp.angle)
			let targetY = pe.ctp.distance * sin(pe.ctp.angle)
			push()
			translate(pe.vertexA[0],pe.vertexA[1])
			line(0, 0, targetX, targetY)
			rotate(pe.ctp.angle)
			pop()
		}*/

		// Reverse Edge rendering
		/*for ( let iTopo in this.topo){
			let pe = this.polarEdges[int(iTopo)+int(this.topo.length)]
			let targetX = pe.ctp.distance * cos(pe.ctp.angle)
			let targetY = pe.ctp.distance * sin(pe.ctp.angle)
			push()
			translate(pe.vertexA[0],pe.vertexA[1])
			line(0, 0, targetX, targetY)
			rotate(pe.ctp.angle)
			pop()
		}*/

	}

}
