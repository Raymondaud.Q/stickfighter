class Stickman {

  toPolarEdges(){
    let polarEdges = []
    for ( let iTopo in this.topo){
      let vertA = this.vertices[this.topo[iTopo][0]]
      let vertB = this.vertices[this.topo[iTopo][1]]
      let ctp = cartesianToPolar(vertB,vertA)
      let r_ctp = cartesianToPolar(vertA,vertB)
      polarEdges[iTopo]={vertexA:vertA, vertexB:vertB, ctp:ctp}
      polarEdges[int(iTopo)+int(this.topo.length)]={vertexA:vertB, vertexB:vertA, ctp: r_ctp}
    }
    return polarEdges
  }

  angle(cx, cy, ex, ey) {
    var dy = ey - cy;
    var dx = ex - cx;
    var theta = atan2(dy, dx);
    return theta;
  }

  constructor(){
    this.vertices = [
      [251,271,0], // 0 Pied Gauche     
      [256,255,0], // 1 Genoux Gauche   
      [272,270,0], // 2 Pied Droit      
      [267,256,0], // 3 Genoux Droit   
      [260,240,0], // 4 Hanche   
      [267,229,0], // 5 Coude Gauche    
      [271,240,0], // 6 Main Gauche     
      [253,230,0], // 7 Coude Droit     
      [249,242,0], // 8 Main Droite     
      [260,210,1]  // 9 Epaules 
    ]

    this.topo = [
      [0,1],
      [2,3],
      [1,4],
      [3,4],
      [9,4],
      [5,9],
      [7,9],
      [5,6],
      [7,8]
    ]

    //this.polarEdges = this.toPolarEdges()
    this.overredVertex = null;
    this.vertexEdit = null;
    this.selectedVertices = [];
    this.angle = radians(-90);
  }


  setPosition(xy){
    let iPos = [this.vertices[4][0], this.vertices[4][1]]
    let dragX = xy[0] - iPos[0] 
    let dragY = xy[1] - iPos[1]
    for (let iSelected in this.vertices){
      this.vertices[iSelected][0] += dragX
      this.vertices[iSelected][1] += dragY
    }
  }

  getFrame(){
    return {topo: this.topo.map(elem=>elem.map(el=>el)), vertices:this.vertices.map(elem=>elem.map(el=>el))}
  }

  setFrame(frame){
    if (frame != null){
      this.topo = frame.topo
      this.vertices = frame.vertices
    }
    this.angle = radians(-90);
    //this.polarEdges = this.toPolarEdges()
  }

  selectInner(rect){
    this.selectedVertices = []
    for (let i = 0; i < this.vertices.length; i ++ ){
      let currentVertex = this.vertices[i]
      let xa = (rect[0] < rect[2]+rect[0] ? currentVertex[0] >= rect[0] && currentVertex[0] <= rect[2]+rect[0] : currentVertex[0] <= rect[0]  && currentVertex[0] >= rect[2]+rect[0])
      let ya = (rect[1] < rect[3]+rect[1] ? currentVertex[1] >= rect[1] && currentVertex[1] <= rect[1]+rect[3] : currentVertex[1] <= rect[1] && currentVertex[1] >= rect[1]+rect[3])
        if (  xa && ya && currentVertex != undefined){
          this.selectedVertices.push(i)
        }
    }
  }

  setHead(){
    if (this.vertexEdit != null ) 
      this.vertices[this.vertexEdit][2] = ( this.vertices[this.vertexEdit][2]==0? 1:0)
    else if (this.overredVertex != null) 
      this.vertices[this.overredVertex][2] = ( this.vertices[this.overredVertex][2]==0? 1:0)
  }


  mouseMovedd(mouseX,mouseY){
    let dists = []
    for (let i = 0; i < this.vertices.length; i ++ )
      dists[i] = dist(mouseX, mouseY, this.vertices[i][0], this.vertices[i][1])


    let minDistToVertex = Math.min(...dists)
    if ( minDistToVertex < 10 )
      this.overredVertex = dists.indexOf(minDistToVertex)
    else 
      this.overredVertex = null
  }

  mouseDraggedd(mouseX,mouseY){
    if (this.selectedVertices.length > 1 && this.selectedVertices.includes(this.overredVertex)){
      let translationX = mouseX - this.vertices[this.overredVertex][0]
      let translationY = mouseY - this.vertices[this.overredVertex][1]
      for (let iSelected in this.selectedVertices){
        this.vertices[this.selectedVertices[iSelected]][0] += translationX
        this.vertices[this.selectedVertices[iSelected]][1] += translationY 
      }
      return true
    } else if ( this.overredVertex != null){
      this.vertices[this.overredVertex][0] = mouseX
      this.vertices[this.overredVertex][1] = mouseY
      return true
    } else if ( this.overredVertex==null && this.vertexEdit != null && !pressedKeys.includes(CONTROL)){
      if (this.selectedVertices.length > 0)
        this.rotateSelected(mouseX,mouseY)
      else
        this.rotate(mouseX, mouseY)
      return true
    } else if ( this.overredVertex==null && pressedKeys.includes(CONTROL)){
      if (this.vertexEdit != null){
        this.vertices.push([mouseX,mouseY])
        this.topo.push([this.vertexEdit,this.vertices.length-1])
        this.vertexEdit=this.vertices.length-1
      } else if (this.vertexEdit == null ){
        this.vertices.push([mouseX,mouseY])
        this.vertexEdit=this.vertices.length-1
      }
      return true
    }
    //this.polarEdges = this.toPolarEdges()
    return false;
  }


  scale(value){ // Homothetie de rapport value
    if (this.selectedVertices.length > 0 ){
      this.scaleSelected(value)
    } else {
      let barycentre = [0,0]
      for ( let i = 0; i < this.vertices.length; i++){
        barycentre[0] += this.vertices[i][0]
        barycentre[1] += this.vertices[i][1]
      }
      barycentre[0] = barycentre[0]/this.vertices.length
      barycentre[1] = barycentre[1]/this.vertices.length

      for ( let i = 0; i < this.vertices.length; i++){
        this.vertices[i][0] = ((this.vertices[i][0]-barycentre[0])*value)+barycentre[0]
        this.vertices[i][1] = ((this.vertices[i][1]-barycentre[1])*value)+barycentre[1]
      }

    }
  }

  scaleSelected(value){
    let barycentre = [0,0]
    for ( let i = 0; i < this.selectedVertices.length; i++){
      if (this.selectedVertices[i] !== undefined && this.vertices[this.selectedVertices[i]] !== undefined ){
        barycentre[0] += this.vertices[this.selectedVertices[i]][0]
        barycentre[1] += this.vertices[this.selectedVertices[i]][1]
      }
    }
    barycentre[0] = barycentre[0]/this.selectedVertices.length
    barycentre[1] = barycentre[1]/this.selectedVertices.length

    for ( let i = 0; i < this.selectedVertices.length; i++){
      if (this.selectedVertices[i] !== undefined && this.vertices[this.selectedVertices[i]] !== undefined ){
        this.vertices[this.selectedVertices[i]][0] = ((this.vertices[this.selectedVertices[i]][0]-barycentre[0])*value)+barycentre[0]
        this.vertices[this.selectedVertices[i]][1] = ((this.vertices[this.selectedVertices[i]][1]-barycentre[1])*value)+barycentre[1]
      }
    }
  }

  rotate(cx,cy){
    let pivotX = this.vertices[this.vertexEdit][0]
    let pivotY = this.vertices[this.vertexEdit][1]

    let angle = Math.atan2(cy-pivotY, cx-pivotX)
    let cosA = cos(this.angle - angle)
    let sinA = -sin(this.angle - angle)
    this.angle = angle

    for (let iSelected in this.vertices){
      this.vertices[iSelected][0] -= pivotX
      this.vertices[iSelected][1] -= pivotY
      let offsetX = this.vertices[iSelected][0] * cosA - this.vertices[iSelected][1] * sinA 
      let offsetY = this.vertices[iSelected][0] * sinA + this.vertices[iSelected][1] * cosA 
      this.vertices[iSelected][0] = offsetX + pivotX
      this.vertices[iSelected][1] = offsetY + pivotY
    }
  }


  rotateSelected(cx,cy){
    if ( this.selectedVertices.length > 0 ){
      let pivotX = this.vertices[this.vertexEdit][0]
      let pivotY = this.vertices[this.vertexEdit][1]

      let angle = Math.atan2(cy-pivotY, cx-pivotX)
      let cosA = cos(this.angle - angle)
      let sinA = -sin(this.angle - angle)
      this.angle = angle

      for (let iSelected in this.selectedVertices){
        this.vertices[this.selectedVertices[iSelected]][0] -= pivotX
        this.vertices[this.selectedVertices[iSelected]][1] -= pivotY
        let offsetX = this.vertices[this.selectedVertices[iSelected]][0] * cosA - this.vertices[this.selectedVertices[iSelected]][1] * sinA 
        let offsetY = this.vertices[this.selectedVertices[iSelected]][0] * sinA + this.vertices[this.selectedVertices[iSelected]][1] * cosA 
        this.vertices[this.selectedVertices[iSelected]][0] = offsetX + pivotX
        this.vertices[this.selectedVertices[iSelected]][1] = offsetY + pivotY
      }
    }
  }


  drag(offsetX, offsetY){
    if ( this.selectedVertices.length > 0 ){
      for (let iSelected in this.selectedVertices){
        this.vertices[this.selectedVertices[iSelected]][0] += offsetX
        this.vertices[this.selectedVertices[iSelected]][1] += offsetY 
      }
    }
    //this.polarEdges = this.toPolarEdges()
  }

  dragg(offsetX, offsetY){
    for (let i = 0; i < this.vertices.length; i ++){
      this.vertices[i][0] += offsetX
      this.vertices[i][1] += offsetY 
    }
  }

  mouseClickedd(event){
    if (this.overredVertex != null ){
      if (pressedKeys.includes(CONTROL) && this.vertexEdit == null){
        if (this.selectedVertices.includes(this.overredVertex)) 
          this.selectedVertices = this.selectedVertices.filter( value => value != this.overredVertex)
        else {
          this.selectedVertices.push(this.overredVertex)
        }
      } else if (this.vertexEdit != null && pressedKeys.includes(CONTROL) )
        this.topo.push([this.vertexEdit,this.overredVertex])
      else if (this.vertexEdit == null ){
        this.vertexEdit = this.overredVertex
      } else if (this.vertexEdit == this.overredVertex ){
        this.vertexEdit = null
      } else if (this.vertexEdit != null) {
        this.vertexEdit = this.overredVertex
      }
    } else if (this.overredVertex == null){
      if (this.vertexEdit != null && pressedKeys.includes(CONTROL)){
        this.vertices.push([mouseX,mouseY])
        this.topo.push([this.vertexEdit,this.vertices.length-1])
        this.vertexEdit=this.vertices.length-1
      } else if (this.vertexEdit == null && pressedKeys.includes(CONTROL) ){
        this.vertices.push([mouseX,mouseY])
        this.vertexEdit=this.vertices.length-1
      } else if (this.vertexEdit =! null && !pressedKeys.includes(CONTROL) ){
        this.vertexEdit = null;
      }
    }
  }

  remove(){
    if (this.vertexEdit != null)
      this.removeVertex(this.vertexEdit)
    while (this.selectedVertices != null && this.selectedVertices.length != 0)
      this.removeVertex(this.selectedVertices[0])
  }

  removeVertex(index){
    let del = index
    this.topo = this.topo.filter(value => value[0] != del && value[1] != del)
    for ( let j = 0 ; j < this.topo.length ; j++){
      if (this.topo[j][0] >= del)
        this.topo[j][0] -= 1
      if (this.topo[j][1] >= del)
        this.topo[j][1] -= 1
    }
    for ( let j = 0 ; j < this.selectedVertices.length ; j++){
      if (this.selectedVertices[j] > del )
        this.selectedVertices[j] -= 1
    }
    this.vertices.splice(del,1)
    this.selectedVertices.splice(this.selectedVertices.indexOf(del),1)
    this.overredVertex = (this.overredVertex == del ? null : this.overredVertex)
    this.vertexEdit = (this.vertexEdit == del ? null : this.vertexEdit)
  }

  lineCoeff(a,b){
    let result = [];
    result[0] = b[1] - a[1]  // a
    result[1] = a[0] - b[0]  // b
    result[2] = - result[0] *  a[0] - result[1] * a[1];
    return result;
  }
  
  symmetrical() {
    let abc = this.lineCoeff([0,0],[0,500]);
    for ( let count = 0 ; count < this.vertices.length; count ++ ) {
      this.vertices[count] = [
      520 + int( -2 * abc[0] * ( abc[0] * this.vertices[count][0] + abc[1] * this.vertices[count][1] + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + this.vertices[count][0]),
       int( -2 * abc[1] * ( abc[0] * this.vertices[count][0] + abc[1] * this.vertices[count][1] + abc[2] ) / ( abc[0]*abc[0] + abc[1]*abc[1] ) + this.vertices[count][1]),
       this.vertices[count][2]
      ];
    }        
  }


  randomPosition(){
    let randomA = random(-0.5,0.5)
    let randomB = random(-0.5,0.5)
    for (let iTopo in this.topo){
      this.vertices[this.topo[iTopo][0]][0] += randomA
      this.vertices[this.topo[iTopo][0]][1] += randomB
      this.vertices[this.topo[iTopo][1]][0] += randomA
      this.vertices[this.topo[iTopo][1]][1] += randomB
    }
  }

  show(){
    for ( let iTopo in this.topo){
      let vertA = this.vertices[this.topo[iTopo][0]]
      let vertB = this.vertices[this.topo[iTopo][1]]
      if (vertA != null && vertB != null) line(vertA[0], vertA[1], vertB[0], vertB[1])
    }

    for ( let iVert in this.vertices)
      if (this.vertices[iVert][2] == 1)
        circle(this.vertices[iVert][0],this.vertices[iVert][1]-5,10)

    fill("#b75955")
    if ( this.overredVertex != null && this.vertices[this.overredVertex] != null)
      circle(this.vertices[this.overredVertex][0], this.vertices[this.overredVertex][1], 10 )

    fill("#097fc9")
    if ( this.vertexEdit!=null && this.vertices[this.vertexEdit])
      circle(this.vertices[this.vertexEdit][0], this.vertices[this.vertexEdit][1], 10 )

    fill("#ff7140")
    for (let iSelected in this.selectedVertices ){
      if (this.selectedVertices[iSelected] != null && this.vertices[this.selectedVertices[iSelected]] != null) 
        circle(this.vertices[this.selectedVertices[iSelected]][0], this.vertices[this.selectedVertices[iSelected]][1], 5 )
      else 
        this.selectedVertices.splice(iSelected,1)
    }

    fill(0)
    // Animation par modification d'angle en mode Polar Edge
    /*let randomA = random(-0.1,0.1)
    let randomB = random(-0.1,0.1)
    this.polarEdges = this.toPolarEdges()
    for (let iTopo in this.topo){
      let pet = this.polarEdges[iTopo]
      pet.ctp.angle+=randomA
      let targetX = pet.ctp.distance * cos(pet.ctp.angle)
      let targetY = pet.ctp.distance * sin(pet.ctp.angle)
      this.vertices[this.topo[iTopo][1]][0] = targetX + this.vertices[this.topo[iTopo][0]][0]
      this.vertices[this.topo[iTopo][1]][1] = targetY + this.vertices[this.topo[iTopo][0]][1]

      pet = this.polarEdges[int(iTopo)+int(this.topo.length)]
      pet.ctp.angle+=randomB
      targetX = pet.ctp.distance * cos(pet.ctp.angle)
      targetY = pet.ctp.distance * sin(pet.ctp.angle)
      this.vertices[this.topo[iTopo][0]][0] = targetX + this.vertices[this.topo[iTopo][1]][0]
      this.vertices[this.topo[iTopo][0]][1] = targetY + this.vertices[this.topo[iTopo][1]][1]
    }*/

    // Normal Edge rendering
    /*for ( let iTopo in this.topo){
      let pe = this.polarEdges[iTopo]
      let targetX = pe.ctp.distance * cos(pe.ctp.angle)
      let targetY = pe.ctp.distance * sin(pe.ctp.angle)
      push()
      translate(pe.vertexA[0],pe.vertexA[1])
      line(0, 0, targetX, targetY)
      rotate(pe.ctp.angle)
      pop()
    }*/

    // Reverse Edge rendering
    /*for ( let iTopo in this.topo){
      let pe = this.polarEdges[int(iTopo)+int(this.topo.length)]
      let targetX = pe.ctp.distance * cos(pe.ctp.angle)
      let targetY = pe.ctp.distance * sin(pe.ctp.angle)
      push()
      translate(pe.vertexA[0],pe.vertexA[1])
      line(0, 0, targetX, targetY)
      rotate(pe.ctp.angle)
      pop()
    }*/

  }

}
