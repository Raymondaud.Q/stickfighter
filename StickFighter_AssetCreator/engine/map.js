class StickMap {

  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.stick = new Stickman()
  }

  start() {}

  clear() {}
  
  clearVehicles(){}

  mouseMovedd(mouseX,mouseY){
    this.stick.mouseMovedd(mouseX,mouseY)
  }

  mouseDraggedd(mouseX, mouseY){
    return this.stick.mouseDraggedd(mouseX,mouseY)
  }

  drag(offsetX, offsetY){
    this.stick.drag(offsetX, offsetY)
  }

  mouseClickedd(mouseX,mouseY){
    this.stick.mouseClickedd(mouseX,mouseY)
  }

  selectInner(rect){
    this.stick.selectInner(rect)
  }

  getFrame(){
    return this.stick.getFrame()
  }

  setFrame(frame){
    this.stick.setFrame(frame)
  }

  scale(ratio){
    this.stick.scale(ratio)
  }

  getSelectedData(){
    return this.stick.getFrame()
  }

  setHead(){
    this.stick.setHead()
  }

  remove(){
    this.stick.remove()
  }

  run() {
    this.stick.show() 
  }
}