var netHandler = new WebSocketHandler("localhost:3000");  
var stickmap;
var pressedKeys = []
var frames = []
var rec 
let playMode = 0;
let play = 0;
let frameIndex=0
let runFrameIndex=0;
let frameOut=false;
let frameLimitSlider;
let frameSkipped=0;
let selectedAnim=S;
let selecting=[];
let mousePos=[0,0]
let skip=false
let allFrames=false;
let showGrid=true;

let btnInterpolation;
let btnSave;
let btnUpld;
let btnRec;

let btnAddFrame;
let btnClear;
let btnNext;
let btnPrevious;
let btnScalePlus;
let btnScaleLess;
let btnPlay;
let btnPlayMode;
let btnRemoveFrame;
let btnRandomPosition;
let btnShowGrid

let btnUP;
let btnDOWN;
let btnLEFT;
let btnRIGHT;

let btnS;
let btnD;
let btnF;

let btnFrameSelection;
let btnMirrorCurrentFrame;

let btnDict={};
let dotImg;
let gateImg;
let recFlag = false
let canvasWidthHeight = 500
let btnLimitsX = 170 
let dragCount = 0 
let dragCountLimitTrigger = 6
let displayGrid = true

let framePreviewSliderX;
let framePreviewSliderY;


function broadcastState(){
  netHandler.send(JSON.stringify({
    frames:frames
  }))
}

function startStopRecord() {

  if(!recFlag){
    recFlag = true
    const chunks = []; // here we will store our recorded media chunks (Blobs)
    const stream = canvas.captureStream(); // grab our canvas MediaStream
    rec = new MediaRecorder(stream); // init the recorder
    // every time the recorder has new data, we will store it in our array
    rec.ondataavailable = e => {chunks.push(e.data);}
    // only when the recorder stops, we construct a complete Blob from all the chunks
    rec.onstop = e => exportVid(new Blob(chunks, {type: 'video/webm'}));
    rec.start();
    setTimeout(()=>rec.stop(), 30000); // stop recording in 20s
    btnRec.elt.textContent="[ALT] Stop 🟥"
  } else {
    rec.stop()
    recFlag = false
    btnRec.elt.textContent="[ALT] Rec 🔴"
  }
}

function exportVid(blob) {
  const vid = document.createElement('video');
  vid.src = URL.createObjectURL(blob);
  vid.controls = true;
  //document.body.appendChild(vid);
  const a = document.createElement('a');
  a.download = 'stickclip.webm';
  a.href = vid.src;
  a.click()
}


function download(data,filename,extension=".json"){
    let file = new Blob([data], {type: "json"});
    filename += extension
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}

function handleFile(files) {
 var file = files.file;
  if (!file) {
    return;
  }
  var reader = new FileReader();
  reader.onload = function(e) {
    var contents = e.target.result;
    frames[selectedAnim]=JSON.parse(contents)
    frameIndex = 0
    let currentFrameDeepCopy = JSON.parse(JSON.stringify(frames[selectedAnim][frameIndex]))
    stickmap.setFrame(currentFrameDeepCopy)
  };
  reader.readAsText(file);
}

function removeFrame(){
  if (frames[selectedAnim]!=null){
    let currentFrameDeepCopy = JSON.parse(JSON.stringify(frames[selectedAnim][frameIndex]))
    frames[selectedAnim].splice(frameIndex,1)
    stickmap.setFrame(currentFrameDeepCopy)
  }
  broadcastState()
}

function insertFrame(){
  if (frames[selectedAnim]!=null){
    let currentFrameDeepCopy = JSON.parse(JSON.stringify(frames[selectedAnim][frameIndex]))
    stickmap.setFrame(currentFrameDeepCopy)
    frames[selectedAnim].splice(frameIndex, 0, currentFrameDeepCopy)
  }
  broadcastState()
}

function randn_bm(min, max, skew) {
  let u = 0, v = 0;
  while(u === 0) u = Math.random() //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random()
  let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v )
  
  num = num / 10.0 + 0.5 // Translate to 0 -> 1
  if (num > 1 || num < 0) 
    num = randn_bm(min, max, skew) // resample between 0 and 1 if out of range
  
  else{
    num = Math.pow(num, skew) // Skew
    num *= max - min // Stretch to fill range
    num += min // offset to min
  }
  return num
}

function cartesianToPolar(vertex, vertexB){
  let x = vertex[0]-vertexB[0]
  let y = vertex[1]-vertexB[1]
  let angle = atan2(y,x)
  let distance = sqrt(x*x + y*y)
  return {tX:x, tY:y, distance: distance, angle:angle}
}

function setup() {

  let canvas = createCanvas(canvasWidthHeight, canvasWidthHeight);
  strokeWeight(2);
  //Stroke with a semi-transparent white
  //stroke(255, 160);
  //angleMode("DEGREES")
  textFont(fontStr)
  for (let val in keyCodeLst){
    frames[keyCodeLst[val]]= new Array()
  }
  frameRate(30);
  init(); 
}

function setCopiedFrame(){
  if (frames[selectedAnim].length <= 0) return
  let implicitIndex = frames[selectedAnim].length == frameIndex ? frameIndex-1 : frameIndex
  if (frames[selectedAnim][implicitIndex] == null) return 
  let currentFrameDeepCopy = JSON.parse(JSON.stringify(frames[selectedAnim][implicitIndex]))
  stickmap.setFrame(currentFrameDeepCopy)
}

function btnStyle(btn){
  btn.style('font-weight',"bold")
  btn.style('border-radius',"20px")
  btn.style('border-color',"grey")
  btn.style('border-width',"0.1px")
  btn.style('font-family',fontStr)
  btn.style('background-color', "black")
  btn.style('color',"white")
}

function init() {

  console.log("HeYo H4ck3r ! \nAfter having selected the canvas element with the inspector,\nYou can access following var : [ \n frames, \n stickmap, \n playMode, \n play,  \n runOut, \n frameLimitSlider, \n frameSkipped, \n frameIndex, \n runFrameIndex, \n pressedKeys, \n ... \n]")
  stickmap = new StickMap(screen.width, screen.height);

  frameLimitSlider = createSlider(0, 100, 0);
  frameLimitSlider.position(45, 25);
  frameLimitSlider.size(110)

  frames[S]=preFilledS
  frames[D]=preFilledD
  frames[F]=preFilledF
  frames[LEFT]=preFilledLeft
  frames[RIGHT]=preFilledRight
  frames[DOWN]=preFilledDown
  frames[UP]=preFilledUp

  btnInterpolation = createButton("Interpolation")
  btnInterpolation.mousePressed(()=>{window.alert(`
  Feed the Meowww
      ／l、             
（ﾟ､ ｡ ７         
  l  ~ヽ       
  じしf_,)ノ
  in order to interpolate`)});
  btnInterpolation.position(5, 5);
  btnInterpolation.size(150, 20);
  btnStyle(btnInterpolation)

  btnAddFrame = createButton("[A] " + (frameIndex==frames[selectedAnim].length ?'Add Frame' : "Replace Frame"))
  btnAddFrame.mousePressed(() => {addFrame(frameIndex == frames[selectedAnim].length)});
  btnAddFrame.position(5, 130);
  btnAddFrame.size(150, 20);
  btnStyle(btnAddFrame)

  btnClear = createButton('[O] Clear Frames');
  btnClear.mousePressed(clearFrames);
  btnClear.position(5, 170);
  btnClear.size(150, 20);
  btnStyle(btnClear)

  btnRemoveFrame = createButton('[R] Remove Frame');
  btnRemoveFrame.mousePressed(removeFrame);
  btnRemoveFrame.position(5, 150);
  btnRemoveFrame.size(150, 20);
  btnStyle(btnRemoveFrame)

  btnPrevious = createButton('[G] Pre.');
  btnPrevious.mousePressed(()=>{shiftFrameIndex(-1)});
  btnPrevious.position(5, 290);
  btnPrevious.size(75, 20);
  btnStyle(btnPrevious)

  btnNext = createButton('[H] Next');
  btnNext.mousePressed(()=>{shiftFrameIndex(1)});
  btnNext.position(80, 290);
  btnNext.size(75, 20);
  btnStyle(btnNext)

  framePreviewSliderX = createSlider(0, 300, 0);
  framePreviewSliderX.position(5, 250);
  framePreviewSliderX.size(70);

  framePreviewSliderY = createSlider(0, 300, 150);
  framePreviewSliderY.position(80, 250);
  framePreviewSliderY.size(70);


  btnPlay = createButton("[P] "+(play ? "STOP":"PLAY"));
  btnPlay.mousePressed(togglePlay);
  btnPlay.position(5, 90);
  btnPlay.size(75, 20);
  btnStyle(btnPlay)

  btnPlayMode = createButton("[M] " + (playMode==2 ? "STD":(playMode==1 ? "BOOM":"LOOP")));
  btnPlayMode.mousePressed(togglePlayMode);
  btnPlayMode.position(80, 90);
  btnPlayMode.size(75, 20);
  btnStyle(btnPlayMode)

  btnInsFrame = createButton("[I] Copy Frame");
  btnInsFrame.mousePressed(insertFrame);
  btnInsFrame.position(5, 110);
  btnInsFrame.size(150, 20);
  btnStyle(btnInsFrame)


  btnUP = createButton(" [⇑] ")
  btnUP.mousePressed(()=>{changeAnim(UP)})
  btnUP.position(95,435)
  btnUP.size(30,30)
  btnStyle(btnUP)

  btnDOWN = createButton("[⇓]")
  btnDOWN.mousePressed(()=>{changeAnim(DOWN)})
  btnDOWN.position(95,465)
  btnDOWN.size(30,30)
  btnStyle(btnDOWN)

  btnLEFT = createButton("[⇐]")
  btnLEFT.mousePressed(()=>{changeAnim(LEFT)})
  btnLEFT.position(65,465)
  btnLEFT.size(30,30)
  btnStyle(btnLEFT)

  btnRIGHT = createButton("[⇒]")
  btnRIGHT.mousePressed(()=>{changeAnim(RIGHT)})
  btnRIGHT.position(125,465)
  btnRIGHT.size(30,30)
  btnStyle(btnRIGHT)

  btnRandomPosition = createButton("🤯")
  btnRandomPosition.mousePressed(()=>{
    this.stickmap.stick.randomPosition()
  })
  btnRandomPosition.position(175,0)
  btnRandomPosition.size(20,20)
  btnStyle(btnRandomPosition)

  btnShowGrid = createButton("𝄜")
  btnShowGrid.mousePressed(()=>{
    showGrid=!showGrid
  })
  btnShowGrid.position(195,0)
  btnShowGrid.size(20,20)
  btnStyle(btnShowGrid)


  btnFrameSelection = createButton("[Shift]-" + (allFrames ?  "Current" : "All frames"))
  btnFrameSelection.mousePressed(()=>{
    allFrames = !allFrames
    btnFrameSelection.elt.textContent = ("[Shift]-" + (allFrames ?  "Current" : "All frames"))

  })
  btnFrameSelection.position(5,190)
  btnFrameSelection.size(150,20)
  btnStyle(btnFrameSelection)


  btnMirrorCurrentFrame = createButton("[W] Mirror effect")
  btnMirrorCurrentFrame.mousePressed(()=>{
    if (!allFrames)
      symmetric()
    else
      symmetricall()
  })
  btnMirrorCurrentFrame.position(5, 210)
  btnMirrorCurrentFrame.size(150,20)
  btnStyle(btnMirrorCurrentFrame)


  btnScaleLess = createButton('[K]Size-');
  btnScaleLess.mousePressed(()=>{
    if (!allFrames)
      stickmap.scale(0.95)
    else
      scalecall(false)
  });
  btnScaleLess.position(5, 230);
  btnScaleLess.size(75, 20);
  btnStyle(btnScaleLess)

  btnScalePlus = createButton('[L]Size+');
  btnScalePlus.mousePressed(()=>{
    if (!allFrames)
      stickmap.scale(1.05)
    else
      scalecall(true)
  });
  btnScalePlus.position(80, 230);
  btnScalePlus.size(75, 20);
  btnStyle(btnScalePlus)

  btnS = createButton("[S]")
  btnS.mousePressed(()=>{changeAnim(S)})
  btnS.position(0, 430)
  btnS.size(30,30)
  btnStyle(btnS)

  btnD = createButton("[D]")
  btnD.mousePressed(()=>{changeAnim(D)})
  btnD.position(30,430)
  btnD.size(30,30)
  btnStyle(btnD)

  btnF = createButton("[F]")
  btnF.mousePressed(()=>{changeAnim(F)})
  btnF.position(60,430)
  btnF.size(30,30)
  btnStyle(btnF)

  btnSave = createButton("[Q] Save Frames")
  btnSave.mousePressed(()=>{download([JSON.stringify(frames[selectedAnim])],"move-"+selectedAnim)})
  btnSave.position(5,380)
  btnSave.size(150,20)
  btnStyle(btnSave)

  btnUpld = createFileInput(handleFile)
  btnUpld.position(5,400)
  btnUpld.size(150,30)

  btnRec = createButton("[ALT] Rec 🔴")
  btnRec.mousePressed(startStopRecord)
  btnRec.position(20,355)
  btnRec.size(120,20)
  btnStyle(btnRec)

  btnDict[UP] = btnUP
  btnDict[DOWN] = btnDOWN
  btnDict[LEFT] = btnLEFT
  btnDict[RIGHT] = btnRIGHT
  btnDict[S] = btnS
  btnDict[D] = btnD
  btnDict[F] = btnF

  changeAnim(S)
  setCopiedFrame()
  togglePlay()
}

function changeAnim(code){
  selectedAnim=code
  frameIndex=0
  if(play)togglePlay()
  for (let btn in btnDict ){
    if (btnDict[code] == btnDict[btn])
      btnDict[code].style('background-color', "red")
    else 
      btnDict[btn].style('background-color', "black")
  }
  setCopiedFrame()
}

function shiftFrameIndex(shift){

  if (play) togglePlay()
  let tmpFrameIndex = frameIndex;

  if ( shift < 0 && frameIndex > 0)
    frameIndex--
  else if ( shift > 0 && frameIndex < frames[selectedAnim].length && frames[selectedAnim].length != 0 )
    frameIndex++
  setCopiedFrame()
}

function symmetricall(){
  if (play) togglePlay()
  let currentFrameDeepCopy = JSON.parse(JSON.stringify(stickmap.getFrame()))
  for (let i in frames[selectedAnim]){
      stickmap.stick.setFrame(frames[selectedAnim][i])
      stickmap.stick.symmetrical()
  }
  stickmap.stick.setFrame(currentFrameDeepCopy)
  stickmap.stick.symmetrical()
  broadcastState()
}

function scalecall(plus){
  if (play) togglePlay()
  let currentFrameDeepCopy = JSON.parse(JSON.stringify(stickmap.getFrame()))
  for (let i in frames[selectedAnim]){
      stickmap.stick.setFrame(frames[selectedAnim][i])
      if (plus) 
        stickmap.scale(1.05)
      else 
        stickmap.scale(0.95)
  }
  stickmap.stick.setFrame(currentFrameDeepCopy)
  if (plus) 
    stickmap.scale(1.05)
  else 
    stickmap.scale(0.95)
  broadcastState()
}

function symmetric(){
  if (play) togglePlay()
  stickmap.stick.symmetrical()
}

function getFrame(){
  frames[selectedAnim][frameIndex]=stickmap.getFrame()
}


function addFrame(shift=true){
  getFrame()
  if (frameIndex+1 == frames[selectedAnim].length && shift)
    shiftFrameIndex(1)
  broadcastState()
}

function clearFrames(){
  if (play) togglePlay()
  frames[selectedAnim]=[]
  frameIndex=0
  btnPlay.elt.textContent = "[P] " + (play ? "STOP":"PLAY")
}

function togglePlay(){
  runFrameIndex = 0
  frameOut=false
  frameSkipped=0
  if (frames[selectedAnim].length != 0){
    play=(play+1) % 2
    if (play) stickmap.setFrame(frames[selectedAnim][0])

    if (frames[selectedAnim].length != 0 ) {
      frameIndex = (frameIndex < frames[selectedAnim].length ? frameIndex : frames[selectedAnim].length)
      setCopiedFrame()
    }
    btnPlay.elt.textContent = "[P] " + (play ? "STOP":"PLAY")
  }
  stickmap.selectInner([])
}

function togglePlayMode(){
  playMode = (playMode + 1) % 3
  btnPlayMode.elt.textContent = "[M] " + (playMode==2 ? "STD":(playMode==1 ?"BOOM":"LOOP"))
}

function isMouseInRestrictedArea(){
  return mouseX <= btnLimitsX || mouseY <=0 || mouseX >= canvasWidthHeight || mouseY >= canvasWidthHeight

}

function mouseClicked() {
  if (isMouseInRestrictedArea()) return
  if ( skip ||  (stickmap != undefined && mousePos[0] != mouseX && mouseY != mousePos[1])) stickmap.mouseClickedd(event)
  else skip = true
}

function mouseMoved() {
  if (isMouseInRestrictedArea()) return
  if (stickmap != undefined ){
    stickmap.mouseMovedd(mouseX,mouseY)
    if (mouseIsPressed && !play ) mousePos = [mouseX,mouseY]
  }
}

function mouseDragged() {
  if (isMouseInRestrictedArea()) return
  if (dragCount < dragCountLimitTrigger){
    dragCount += 1
    return 
  }
  if (!play && stickmap != undefined){

    if ( selecting.length == 0 ){
      mapTriggerResult = stickmap.mouseDraggedd(mouseX,mouseY)
    }

    let frameRecording = mapTriggerResult && pressedKeys.includes(SHIFT) && ( stickmap.stick.overredVertex != null || stickmap.stick.vertexEdit != null || stickmap.stick.selectedVertices.length > 0 )
    if (frameRecording){
      if(frameSkipped < frameLimitSlider.value())
        frameSkipped+=1
      else {
        let editedFrame = stickmap.getFrame()


          if (frames[selectedAnim][frameIndex] == null ){
            frames[selectedAnim][frameIndex] = editedFrame
          } else {
            for (let index in frames[selectedAnim][frameIndex].vertices){
              editedFrame.vertices[index] = frames[selectedAnim][frameIndex].vertices[index]
            }
            for (let index in frames[selectedAnim][frameIndex].topo){
              editedFrame.topo[index] = frames[selectedAnim][frameIndex].topo[index]
            }
            frames[selectedAnim][frameIndex] = editedFrame
            stickmap.stick.setFrame(editedFrame)
          }
        frameIndex += 1;
        frameSkipped = 0 
      }
    }

    if (selecting.length >= 2 &&  ! mapTriggerResult && ! frameRecording){
      selecting[2]=mouseX-selecting[0]
      selecting[3]=mouseY-selecting[1]
      stickmap.selectInner(selecting)
    } else {
      selecting = []
    }

    if ( ! mapTriggerResult && ! frameRecording && selecting.length == 0 && mouseX > 160 ) {
      selecting=[mouseX,mouseY]
    } 
  }
  mousePos = [mouseX,mouseY]
  skip = false
}
  
function mouseReleased() {
  selecting = []
  dragCount = 0
}

function keyPressed(){
  //console.log(keyCode)
  pressedKeys.push(keyCode)
}

function keyReleased(){
  pressedKeys = pressedKeys.filter( value => value != keyCode )
  if (keyCode == A && !play){
    addFrame(frameIndex == frames[selectedAnim].length)
  } else if (keyCode == P){
    togglePlay()
  } else if (keyCode == M){
    togglePlayMode()
  } else if (keyCode == B) {
    frameLimitSlider.value(frameLimitSlider.value()-1)
  } else if (keyCode == N) {
    frameLimitSlider.value(frameLimitSlider.value()+1)
  } else if (keyCode == O){
    clearFrames()
  } else if (keyCode ==  G){
    shiftFrameIndex(-1)
  } else if (keyCode == H){
    shiftFrameIndex(1)
  } else if (keyCode == UP){
    if (stickmap.stick.selectedVertices.length > 0)
      stickmap.drag(0,-1)
    else {
      changeAnim(keyCode)
      if (!play) togglePlay()
    }
  } else if (keyCode == DOWN){
    if (stickmap.stick.selectedVertices.length > 0)
      stickmap.drag(0,1)
    else {
      changeAnim(keyCode)
      if (!play) togglePlay()
    }
  } else if (keyCode == LEFT){
    if (stickmap.stick.selectedVertices.length > 0)
      stickmap.drag(-1,0)
    else {
      changeAnim(keyCode)
      if (!play) togglePlay()
    }
  } else if (keyCode == RIGHT){
    if (stickmap.stick.selectedVertices.length > 0)
      stickmap.drag(1,0)
    else {
      changeAnim(keyCode)
      if (!play) togglePlay()
    }
  } else if (keyCode == Q){
    download([JSON.stringify(frames[selectedAnim])],"move-"+selectedAnim)
  } else if (keyCode == S){
    changeAnim(keyCode)
    if (!play) togglePlay()
  } else if (keyCode == D){
    changeAnim(keyCode)
    if (!play) togglePlay()
  } else if (keyCode == F){
    changeAnim(keyCode)
    if (!play) togglePlay()
  } else if (keyCode == ALT){
    startStopRecord()
  } else if ( keyCode == R && !play){
    removeFrame()
  } else if(!play && keyCode == K ){
     if (!pressedKeys.includes(SHIFT) && !allFrames )
      stickmap.scale(0.95)
    else
      scalecall(false)
  } else if(!play && keyCode == L ){
     if (!pressedKeys.includes(SHIFT) && !allFrames )
      stickmap.scale(1.05)
    else
      scalecall(true)
  } else if ( !play && keyCode == W) {
    if (!pressedKeys.includes(SHIFT) && !allFrames )
      symmetric()
    else
      symmetricall()
  } else if (keyCode == SHIFT){
    allFrames =! allFrames
    btnFrameSelection.elt.textContent = "[Shift]-" + (allFrames ?  "Current frame" : "All frames")
  } else if (!play && keyCode == I){
    insertFrame()
  } else if (!play && ( keyCode == ERASE || keyCode == SUPPR )) {
    this.stickmap.remove()
  } else if (!play && keyCode == ENTER){
    this.stickmap.setHead()
  } else if (keyCode == C && pressedKeys.includes(CONTROL)){
    // Avoids a bug due to loss of canvas focus
    let selectedData = JSON.stringify([stickmap.getSelectedData()])
    pressedKeys = pressedKeys.filter( value => value != CONTROL )
    copyStringToClipboard(selectedData)
    download(selectedData,"data-clipboard")
  }
}

//From https://editor.p5js.org/olafval/sketches/vmclfZbIB
function copyStringToClipboard (str) {
   // Create new element
   var el = document.createElement('textarea');
   // Set value (string to be copied)
   el.value = str;
   // Set non-editable to avoid focus and move outside of view
   el.setAttribute('readonly', '');
   el.style = {position: 'absolute', left: '-9999px'};
   document.body.appendChild(el);
   // Select text inside element
   el.select();
   // Copy text to clipboard
   document.execCommand('copy');
   // Remove temporary element
   document.body.removeChild(el);
}

function gridDecoration(){
  let numIter = 0
  let c = color("grey")
  for (let i = 0; i< canvasWidthHeight; i+=10 ){
    if(numIter%2==0){
      strokeWeight(0.2)
      c.setAlpha(255);
    } else {
      strokeWeight(1)
      c.setAlpha(128);
    }
    stroke(c)
    line(170,i+20,canvasWidthHeight,i+20)
    line(i+170,20,i+170,canvasWidthHeight)
    numIter+=1

  }
}

function guiDecoration(){

  if(showGrid && !recFlag)
    gridDecoration()


  if (!recFlag){
    textFont('Courier New', 12);
    stroke("white")
    text('StickFighter - Asset Creator', 250, 15);
    stroke("black")
    strokeWeight(1)
    fill(200,200,200,255)
    rect(0,0, btnLimitsX, canvasWidthHeight)
    fill(255,255,255,0)
    rect(0,0, canvasWidthHeight, canvasWidthHeight)
    fill(0)
    btnAddFrame.elt.textContent="[A] " + (frameIndex < frames[selectedAnim].length ? "Replace Frame" : 'Add Frame')
    text("[B|N]", 5, 40)
    text("[Shift] Frame each " + int(int(frameLimitSlider.elt.value)+1) + "\n    MouseDrag evt", 5, 55)
    text("[CTRL]+[Click] Select ", 5, 85);
    text("    Frame " + str(frameIndex+1)+" of "+ (frames.length>0 ? str(frames[selectedAnim].length) : "0") ,5, 285)
    text("Click on a vertex then\n Drag around to spin\n    Angle: "+int(degrees(stickmap.stick.angle)+90) ,5, 320)
    if (selecting.length==4){
      fill(0,127,0,40)
      rect(selecting[0],selecting[1],selecting[2],selecting[3])
    }

  }

}

function guiComputeState(){
  if(play && frames.length > 0) {
      if ( runFrameIndex < frames[selectedAnim].length) stickmap.setFrame(frames[selectedAnim][runFrameIndex])
      if (playMode == 1){
        runFrameIndex = (runFrameIndex+1)%frames[selectedAnim].length
      } else if ( playMode == 0){
        if (runFrameIndex < frames[selectedAnim].length-1) runFrameIndex += 1
        else togglePlay()
      } else {
        if (runFrameIndex == frames[selectedAnim].length-1) frameOut=true
        else if (runFrameIndex == 0)frameOut=false
        if ( !frameOut ) runFrameIndex+=1
        else runFrameIndex-=1
      }
    }

    if (frames[selectedAnim].length == 0){
      btnPrevious.hide()
      btnNext.hide()
      btnRemoveFrame.hide()
      btnClear.hide()
      btnPlay.hide()
    } else {
      if (frameIndex == 0) btnPrevious.hide()
      else btnPrevious.show()
      if (frameIndex >= frames[selectedAnim].length){
        btnRemoveFrame.hide()
        btnNext.hide()
      } else {
        btnNext.show()
        btnRemoveFrame.show()
      }
      btnClear.show()
      btnPlay.show()
    }

    if (frames[selectedAnim][frameIndex]==null){
      btnInsFrame.hide()
      btnRemoveFrame.hide()
    } else {
      btnInsFrame.show()
      btnRemoveFrame.show()
      if ( !play && JSON.stringify(frames[selectedAnim][frameIndex]) != JSON.stringify(stickmap.stick.getFrame())){
        btnAddFrame.style("color" ,"red")
      } else {
        btnAddFrame.style("color","white")
      }
    }

    if (allFrames){
      btnFrameSelection.style("color", "red")
    } else {
      btnFrameSelection.style("color", "white")
    }
}


function previewNextAndPreviousFrame(){
  if (!play && !recFlag && frames[selectedAnim] != null){
    stroke(255,0,0,50)
    let implicitFrameIndex = frames[selectedAnim].length < frameIndex ? frameIndex-2 : frameIndex-1
    if(frameIndex > 0 && frames[selectedAnim][implicitFrameIndex] != null){
      let previousFrame = new Stickman()
      previousFrame.setFrame(JSON.parse(JSON.stringify(frames[selectedAnim][implicitFrameIndex])))
      previousFrame.dragg(-int(framePreviewSliderX.elt.value),-int(framePreviewSliderY.elt.value))
      previousFrame.selectedVertices = JSON.parse(JSON.stringify(stickmap.stick.selectedVertices))
      previousFrame.show()
    }
    stroke(0,0,255,50)
    if(frameIndex < frames[selectedAnim].length-1 && frames[selectedAnim][frameIndex+1] != null){
      let nextFrame = new Stickman()
      nextFrame.setFrame(JSON.parse(JSON.stringify(frames[selectedAnim][frameIndex+1])))
      nextFrame.dragg(int(framePreviewSliderX.elt.value),int(framePreviewSliderY.elt.value))
      nextFrame.selectedVertices = JSON.parse(JSON.stringify(stickmap.stick.selectedVertices))
      nextFrame.show()
    }
  }
}


function draw() {

  background(255)
  strokeWeight(1)
  if (stickmap != undefined) {
    guiComputeState()
    strokeWeight(2)
    stickmap.run()
    strokeWeight(2)
    previewNextAndPreviousFrame()
    guiDecoration()
  }
}