// RAYMONDAUD Quentin
// Socket server that share a single quixo game with all clients


const WebSocket = require('ws'); // new


function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

// WebSocket broadcast function
function broadcast(server,data) {
    server.clients.forEach(function each(client) {
        if (client.readyState === WebSocket.OPEN)
            client.send(data);
    });
}

// Exportable serverSocket module
module.exports = function serverSocket(server){
    const serverSocket = new WebSocket.Server({server:server});
    serverSocket.on('connection', (socketClient) => {
        // When a new client is connected, send him the board
        //socketClient.send( "Hello");
        // Client request handling
        socketClient.on('message', function incoming(message) {
            if ( isJson(message)){
                let array = JSON.parse(message)
                broadcast(serverSocket, JSON.stringify(array));
                console.log("VALID - " + array);
            }
            else // if WTF is received just show it  
                console.log("INVALID - " + message)

        });
        // If no client connected to the game, just reset the game
        socketClient.on('close', (socketClient) => {
            if ( serverSocket.clients.size === 0 ){
                console.log("No clients, reinit quixo")
            }
        });

    });
    console.log("WebSocket Server Started")
    return serverSocket
}
