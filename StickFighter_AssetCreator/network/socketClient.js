// Quentin RAYMONDAUD
// Minimal WebSocket Support for shared game

class WebSocketHandler{
    constructor(ipport){
        this.ws = new WebSocket('ws://'+ipport+'/');
        this.ws.onmessage = function (event) { // Handle received data from server
            console.log(event.data);
            frames = JSON.parse(event.data).frames
            let currentFrameDeepCopy = JSON.parse(JSON.stringify(frames[selectedAnim][frameIndex]))
            stickmap.setFrame(currentFrameDeepCopy)
        };
    }

    send(data){
        this.ws.send(data);
    }
}
