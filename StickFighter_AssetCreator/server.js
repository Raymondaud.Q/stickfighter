const http = require('http')
const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
	res.sendFile('index.html', {root: __dirname })
})

app.get('/main', (req,res) => { 
	res.sendFile('main.js', {root: __dirname})
})

app.get('/enums', (req,res) => { 
	res.sendFile('enums.js', {root: __dirname})
})

app.use('/img', express.static('img'))
app.use('/engine', express.static('engine'))
app.use('/network', express.static('network'))
app.use('/lib', express.static('lib'))
app.use('/style', express.static('style'))


const server = http.createServer(app);
server.listen(port, () => {
	console.log(`Example app listening at http://localhost:${port}`)
})

// CREATE WEBSOCKET SERVER
const Socket = require('./socketServer');
serverSocket = Socket(server);
